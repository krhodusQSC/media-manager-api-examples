@echo off
set /P IP="Enter Core IP Address:"
echo Uploading File to Core IP: %IP%
curl -k -X POST https://%IP%/api/v0/cores/self/media/Audio -H "Content-Type: multipart/form-data" -F "media=@C:\Users\Public\Music\Sample Music\Kalimba.mp3"
echo "     "
echo File Uploaded to Core IP: %IP%
pause