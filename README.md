# Media Manager API Example Files #

This repo provides examples for how to utilize the Media Manager API in Q-SYS Core Manager. 

### LoadNinjaTuna.bat ###

Uploads Ninja Tuna (Kalimba - Mr. Scruff) from your PC to the Core's Audio files directory.  

* Requires Ninja Tuna to be installed in C:\Users\Public\Music\Sample Music\Kalimba.mp3
* Requires cURL or Windows 10 version 1803 or later
* Requires Access Control to be disabled on the Core (Maybe someday I'll add it in if there is enough demand.....)


To run, simply download to your PC and launch batch file

### LuaFunctions ###

Example functions that can be utilized within the Q-SYS Scripting Environment to access the Media Manager API 

* These examples are derived from the [Q-SYS Help Manual Media Management API Documentation ](https://q-syshelp.qsc.com/Content/Management_APIs/Management_APIs_Overview.htm)
* Some functions are currently not supported by the HTTPClient library in Q-SYS Designer
* These examples assume that Access Control is enabled on the Core and provide an example of handling Authentication


To run, simply copy and paste the desired function into your Lua script. 
